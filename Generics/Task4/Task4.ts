// How can we fix the code below so that by adding a condition to the
// if statement the right Type Dog or Cat is deducted by the TypeScript compiler?
// You can change the Animal, Dog, and Cat type for this one.

type Animal = {
  age: number;
};

type Dog = Animal & {
  cuddle: () => void;
};

type Cat = Animal & {
  sleep: () => void;
};

// How would you fix this method?
function performAction(animal: Dog | Cat) {
  if (/** */) {
      animal.cuddle();
  }
  if (/** */) {
      animal.sleep();
  }
}

const dog: Dog = {
  age: 7,
  cuddle: () => console.log("cuddling"),
};
const cat: Cat = {
  age: 7,
  sleep: () => console.log("sleeping"),
};

console.log(performAction(dog));
console.log(performAction(cat));
