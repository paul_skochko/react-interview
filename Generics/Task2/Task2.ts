// Given the previous Point3D type and this function:
type NewPoint3D = {
  x: number;
  y: number;
  z: number;
};
function rotateX(x: number, offset: number) {
  return x + offset;
}
// How can you ensure that the function arguments relies on the type
// of NewPoint3D member x instead of using number so that if the NewPoint3D
// member x type is changed, the function argument types are changed as well?
console.log(rotateX(55, 40));
