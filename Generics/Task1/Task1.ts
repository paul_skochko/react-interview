// Given the following type:
type Point3D = {
  x: number;
  y: number;
  z: number;
};
// How do you make an interface Point2D that’s equivalent to Point3D but without have the z attribute?
