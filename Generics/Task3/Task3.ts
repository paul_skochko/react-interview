// How you make this function accept any type without using any or unknown?
function fun(variable: string) {
  return variable;
}
const result1 = fun("string");
const result2 = fun(true);
const result3 = fun(new Date());
console.table({ result1, result2, result3 });
